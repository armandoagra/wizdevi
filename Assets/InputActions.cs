// GENERATED AUTOMATICALLY FROM 'Assets/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""CharacterControls"",
            ""id"": ""d08260c8-b143-4cd3-8213-9a345c38a7db"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""feaea562-6388-478d-8965-e563db9cd9ba"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""d1d0a250-d77b-4b3f-807f-81783909087c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""InventoryKey"",
                    ""type"": ""Button"",
                    ""id"": ""4845e348-a9ba-45bf-bab3-4fcb2f942e79"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""35a4cedc-0d77-4dfd-9a66-d37b982532e6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleRun"",
                    ""type"": ""Button"",
                    ""id"": ""040208a1-5100-4166-a2fc-2af334e22d14"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraSpeed"",
                    ""type"": ""Button"",
                    ""id"": ""6bbdfc7e-502d-4b68-807f-59c9dd3e8218"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Swap"",
                    ""type"": ""Button"",
                    ""id"": ""01071b18-f1ba-4fb7-8e93-cffdb0344597"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""2d3699ed-bc63-4244-8e8d-01cff8807804"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""fd1af1bc-76fd-4794-9cdb-57b28e02b7fe"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0969c4b3-e97d-4b51-a7dc-4f12f5a6b0c9"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""9d5624a4-4957-417f-abf2-6bd0287436f6"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""73e922c5-c17e-4142-b402-cb7523932846"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""569df00f-e75a-4e21-a0ea-508489290002"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""c93ece26-8434-4d0b-9bc3-b896f6bec08f"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""1b0a656f-3395-4116-995b-e39184afeae6"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""26c119a5-e7dc-43bd-9eea-6ee18220da0e"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d2912b8f-9348-4d3d-83a7-4131a9518c36"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""2ff1e5ea-f4f1-4e1b-9207-e4b8741c9b4c"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""50b1a236-a69b-40b5-9bc4-0ddc732cba8c"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c09d4ffd-c04d-4c9e-8be4-828277fdf029"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a77617f8-56f7-4577-8d18-8c78d79c36f8"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""ToggleRun"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""12c68530-af77-4b67-ab9e-cf9a8b046919"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraSpeed"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""ce2be6ac-8810-4b15-948f-ade3a1328ca9"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""CameraSpeed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c88764e0-1db5-465c-90d5-b6902610363c"",
                    ""path"": ""<Keyboard>/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""CameraSpeed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""233930fd-c8a0-421d-a825-03a2407626c9"",
                    ""path"": ""<Keyboard>/i"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""InventoryKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8ba90ce6-64ad-40f6-a94c-ffe32cfc3919"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""InventoryKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e601cc46-ec12-4c27-9963-b2c262ca3d03"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2c9540af-5763-4083-82a0-19cc948d059c"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""995878d9-896f-41d0-8b1e-8395ede40a27"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad;KBM"",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""423f19c6-0c86-4737-8d58-17e4cfe6f0d8"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""372970e0-c9df-47d5-99d6-50f69197f0b7"",
            ""actions"": [
                {
                    ""name"": ""ChangeSelection"",
                    ""type"": ""Value"",
                    ""id"": ""d1067454-1535-4819-b79b-8209b522ead8"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ButtonClick"",
                    ""type"": ""Button"",
                    ""id"": ""4ddb35fe-7ddc-4b9a-9b59-6b82e15dccd4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""UpDown (Teclado)"",
                    ""id"": ""f8f35ba6-0a4d-40cb-b488-fede9b74fc33"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1b513bec-aac4-4be4-aa6b-12d4f0509717"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""212f3308-d043-4f60-9949-65d84431d3f9"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""UpDown (Dpad)"",
                    ""id"": ""d140e8ec-59c8-44e4-bf5b-9d4edbdafcd7"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""b312930e-9107-495a-aa4a-2df8c0a9e085"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM;Gamepad"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ead69138-4928-4595-a87e-9cabbfc155e8"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM;Gamepad"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""UpDown (Analog)"",
                    ""id"": ""0e4816a9-452f-44af-a599-aefa8cbe701e"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""08713807-2add-469e-ad27-c40729882e9b"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM;Gamepad"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""2a8bb01a-7c32-4a3b-91c9-488a2e3c894b"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KBM;Gamepad"",
                    ""action"": ""ChangeSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""866a7dc5-5003-44cd-9ead-cfe94dc161fa"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9608115f-c9c0-49fd-b34a-9a2beeea7871"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ButtonClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KBM"",
            ""bindingGroup"": ""KBM"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // CharacterControls
        m_CharacterControls = asset.FindActionMap("CharacterControls", throwIfNotFound: true);
        m_CharacterControls_Movement = m_CharacterControls.FindAction("Movement", throwIfNotFound: true);
        m_CharacterControls_Interact = m_CharacterControls.FindAction("Interact", throwIfNotFound: true);
        m_CharacterControls_InventoryKey = m_CharacterControls.FindAction("InventoryKey", throwIfNotFound: true);
        m_CharacterControls_Look = m_CharacterControls.FindAction("Look", throwIfNotFound: true);
        m_CharacterControls_ToggleRun = m_CharacterControls.FindAction("ToggleRun", throwIfNotFound: true);
        m_CharacterControls_CameraSpeed = m_CharacterControls.FindAction("CameraSpeed", throwIfNotFound: true);
        m_CharacterControls_Swap = m_CharacterControls.FindAction("Swap", throwIfNotFound: true);
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_ChangeSelection = m_Menu.FindAction("ChangeSelection", throwIfNotFound: true);
        m_Menu_ButtonClick = m_Menu.FindAction("ButtonClick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // CharacterControls
    private readonly InputActionMap m_CharacterControls;
    private ICharacterControlsActions m_CharacterControlsActionsCallbackInterface;
    private readonly InputAction m_CharacterControls_Movement;
    private readonly InputAction m_CharacterControls_Interact;
    private readonly InputAction m_CharacterControls_InventoryKey;
    private readonly InputAction m_CharacterControls_Look;
    private readonly InputAction m_CharacterControls_ToggleRun;
    private readonly InputAction m_CharacterControls_CameraSpeed;
    private readonly InputAction m_CharacterControls_Swap;
    public struct CharacterControlsActions
    {
        private @InputActions m_Wrapper;
        public CharacterControlsActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_CharacterControls_Movement;
        public InputAction @Interact => m_Wrapper.m_CharacterControls_Interact;
        public InputAction @InventoryKey => m_Wrapper.m_CharacterControls_InventoryKey;
        public InputAction @Look => m_Wrapper.m_CharacterControls_Look;
        public InputAction @ToggleRun => m_Wrapper.m_CharacterControls_ToggleRun;
        public InputAction @CameraSpeed => m_Wrapper.m_CharacterControls_CameraSpeed;
        public InputAction @Swap => m_Wrapper.m_CharacterControls_Swap;
        public InputActionMap Get() { return m_Wrapper.m_CharacterControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CharacterControlsActions set) { return set.Get(); }
        public void SetCallbacks(ICharacterControlsActions instance)
        {
            if (m_Wrapper.m_CharacterControlsActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Interact.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInteract;
                @InventoryKey.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInventoryKey;
                @InventoryKey.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInventoryKey;
                @InventoryKey.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnInventoryKey;
                @Look.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLook;
                @ToggleRun.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnToggleRun;
                @ToggleRun.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnToggleRun;
                @ToggleRun.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnToggleRun;
                @CameraSpeed.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnCameraSpeed;
                @CameraSpeed.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnCameraSpeed;
                @CameraSpeed.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnCameraSpeed;
                @Swap.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnSwap;
                @Swap.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnSwap;
                @Swap.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnSwap;
            }
            m_Wrapper.m_CharacterControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @InventoryKey.started += instance.OnInventoryKey;
                @InventoryKey.performed += instance.OnInventoryKey;
                @InventoryKey.canceled += instance.OnInventoryKey;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @ToggleRun.started += instance.OnToggleRun;
                @ToggleRun.performed += instance.OnToggleRun;
                @ToggleRun.canceled += instance.OnToggleRun;
                @CameraSpeed.started += instance.OnCameraSpeed;
                @CameraSpeed.performed += instance.OnCameraSpeed;
                @CameraSpeed.canceled += instance.OnCameraSpeed;
                @Swap.started += instance.OnSwap;
                @Swap.performed += instance.OnSwap;
                @Swap.canceled += instance.OnSwap;
            }
        }
    }
    public CharacterControlsActions @CharacterControls => new CharacterControlsActions(this);

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_ChangeSelection;
    private readonly InputAction m_Menu_ButtonClick;
    public struct MenuActions
    {
        private @InputActions m_Wrapper;
        public MenuActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @ChangeSelection => m_Wrapper.m_Menu_ChangeSelection;
        public InputAction @ButtonClick => m_Wrapper.m_Menu_ButtonClick;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @ChangeSelection.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnChangeSelection;
                @ChangeSelection.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnChangeSelection;
                @ChangeSelection.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnChangeSelection;
                @ButtonClick.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtonClick;
                @ButtonClick.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtonClick;
                @ButtonClick.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtonClick;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ChangeSelection.started += instance.OnChangeSelection;
                @ChangeSelection.performed += instance.OnChangeSelection;
                @ChangeSelection.canceled += instance.OnChangeSelection;
                @ButtonClick.started += instance.OnButtonClick;
                @ButtonClick.performed += instance.OnButtonClick;
                @ButtonClick.canceled += instance.OnButtonClick;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);
    private int m_KBMSchemeIndex = -1;
    public InputControlScheme KBMScheme
    {
        get
        {
            if (m_KBMSchemeIndex == -1) m_KBMSchemeIndex = asset.FindControlSchemeIndex("KBM");
            return asset.controlSchemes[m_KBMSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface ICharacterControlsActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnInventoryKey(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnToggleRun(InputAction.CallbackContext context);
        void OnCameraSpeed(InputAction.CallbackContext context);
        void OnSwap(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnChangeSelection(InputAction.CallbackContext context);
        void OnButtonClick(InputAction.CallbackContext context);
    }
}

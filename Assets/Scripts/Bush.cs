using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bush : MonoBehaviour
{

    public GameObject particles;
    public float timeToDestroy;


    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")){
            GameObject p = Instantiate(particles, transform.position + Vector3.up, Quaternion.identity);
            Destroy(p, timeToDestroy);
        }
    }
}

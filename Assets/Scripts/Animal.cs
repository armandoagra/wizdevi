using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour
{

    public GameItem itemDrop;
    public bool hasDropped;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DropItem(){
        if (hasDropped){ 
            return;
        }
        hasDropped = true;
        Instantiate(itemDrop.gameObject, transform.position, Quaternion.identity);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using System.Linq;
using UnityEngine.InputSystem.Controls;

public class PlayerMovement : MonoBehaviour
{
    public float velocidade;
    public Vector2 _look;
    public Vector2 _move;
    public Quaternion nextRotation;
    public float rotationPower = 3f;
    public float rotationLerp = 0.5f;
    public Transform followTransform;
    public Vector3 nextPosition;
    private Vector2 smoothDeltaPosition = Vector2.zero;
    public Vector2 velocity = Vector2.zero;
    public float magnitude = 0.25f;
    bool shouldMove;
    public Camera cam;
    PlayerInput playerInput;
    public GameItem nearbyObject;
    public Animator animator;
    float _running;
    bool isRunning = true;
    public float velocidadeMax;
    CharacterController cc;
    public float gravity = -9.81f;
    float turnSmoothVelocity;
    public bool insideHouse;
    public Witch witch;
    public Animal nearbyAnimal;
    public bool isNextToWitch;
    public bool inHighGrass;
    public ParticleSystem defaultPS;
    public ParticleSystem[] allPS;
    ParticleSystem currentPS;
    ParticleSystem.EmissionModule em;
    public float highGrassThreshold;
    bool isRunningController;
    public GameObject playerCamera, cutsceneCamera, cutsceneCamera2;
    public Animacoes itemAnim;
    public string currentControl;
    public CinemachineInputProvider provider;
    public InputActionReference controllerXY, mouseXY;
    public InputBinding lastBinding;
    public InputActionAsset iaa;
    InputControl[] blockedButtons;
    public enum Animacoes
    {
        Idle,
        Walking,
        Running,
        Sniffing,
        Barking,
        Digging
    };

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        currentPS = defaultPS;
        playerInput = GetComponent<PlayerInput>();
        currentControl = playerInput.currentControlScheme;
        SwapInput(currentControl);
        GameManager.instance.UpdateUIByInputDevice(currentControl);
        if (Gamepad.current != null)
        {
            blockedButtons = new InputControl[8]
            {
            Gamepad.current.leftStick.left,  Gamepad.current.leftStick.right,
            Gamepad.current.leftStick.up, Gamepad.current.leftStick.down,
            Gamepad.current.rightStick.left, Gamepad.current.rightStick.right,
            Gamepad.current.rightStick.up, Gamepad.current.rightStick.down
            };
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.inCutscene) { return; }
        //CameraRotation();
        Movement();
        if (Keyboard.current.anyKey.wasPressedThisFrame)
        {
            SwapInput("KBM");
        }
        if (Mouse.current.leftButton.wasPressedThisFrame || Mouse.current.rightButton.wasPressedThisFrame)
        {
            SwapInput("KBM");
        }

        if (Gamepad.current != null)
        {
            for (int i = 0; i < Gamepad.current.allControls.Count; i++)
            {
                InputControl c = Gamepad.current.allControls[i];
                if (c is ButtonControl && !blockedButtons.Contains<InputControl>(c))
                {
                    if (((ButtonControl)c).wasPressedThisFrame)
                    {
                        SwapInput("Gamepad");
                    }
                }
            }
        }
    }

    void CameraRotation()
    {
        // Camera rotation

        followTransform.transform.rotation *= Quaternion.AngleAxis(_look.x * rotationPower * Time.deltaTime, Vector3.up);
        followTransform.transform.rotation *= Quaternion.AngleAxis(-_look.y * rotationPower * Time.deltaTime, Vector3.right);

        // Clamping
        var angles = followTransform.transform.localEulerAngles;
        angles.z = 0;
        var angle = followTransform.transform.localEulerAngles.x;
        if (angle > 180 && angle < 340)
        {
            angles.x = 340;
        }
        else if (angle < 180 && angle > 40)
        {
            angles.x = 40;
        }
        // return;
        // Rotating follow object
        followTransform.transform.localEulerAngles = angles;
        nextRotation = Quaternion.Lerp(followTransform.transform.rotation, nextRotation, Time.deltaTime * rotationLerp);
        if (_move.x == 0 && _move.y == 0)
        {
            nextPosition = transform.position;
            return;
        }
        // Set the player rotation based on the look transform
        transform.rotation = Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0);
        // transform.rotation = Quaternion.Lerp(transform.rotation, 
        // Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0), Time.deltaTime/3f);
        // Reset the y rotation of the look transform
        followTransform.transform.localEulerAngles = new Vector3(angles.x, 0, 0);
    }
    void Movement()
    {
        if (isRunning) { velocidade = velocidadeMax; } else { velocidade = velocidadeMax / 3f; }
        // Vector3 movement = (_move.x * transform.right + _move.y * transform.forward).normalized * velocidade;
        // Vector3 movement = new Vector3(_move.x, 0, _move.y).normalized * velocidade;
        float animSpeed = 0f;
        if (playerInput.currentControlScheme == "KBM")
        {
            if (isRunning && _move.magnitude > 0f)
            {
                animSpeed = 1f;
            }
            else if (!isRunning && _move.magnitude > 0f)
            {
                animSpeed = 0.5f;
            }
        }
        else
        {
            animSpeed = _move.magnitude;
            if (_move.magnitude > highGrassThreshold)
            {
                isRunningController = true;
            }
            else
            {
                isRunningController = false;
            }
        }
        Vector3 movement = new Vector3(_move.x, 0, _move.y) * velocidade;
        animator.SetFloat("Speed", animSpeed);
        bool hopping = false;
        if (playerInput.currentControlScheme == "Gamepad")
        {
            if (inHighGrass && isRunningController) { hopping = true; }
            else { hopping = false; }
        }
        else
        {
            if (inHighGrass && isRunning) { hopping = true; }
            else { hopping = false; }
        }
        animator.SetBool("InHighGrass", hopping);
        // if (Input.GetButtonDown("Jump") && cc.isGrounded)
        // {
        //     movement.y = jumpForce;
        //     // animator.SetBool("isJumping", true);
        //     // animator.SetTrigger("jump");
        // }
        Vector3 moveDir = Vector3.zero;
        if (movement.magnitude > 0f)
        {
            float targetAngle = Mathf.Atan2(movement.x, movement.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, 0.1f);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        }
        if (!cc.isGrounded)
        {
            movement.y += gravity;
        }

        // movement = moveDir * velocidade + Vector3.up * movement.y;
        float speedMultiplier = Mathf.Clamp01(_move.magnitude);
        movement = moveDir * velocidade * speedMultiplier + Vector3.up * movement.y;
        // movement.Set(movement.x * Mathf.Abs(_move.x), movement.y, movement.z * Mathf.Abs(_move.y));

        cc.Move(movement * Time.deltaTime);
    }

    public void OnMovement(InputValue value)
    {
        _move = value.Get<Vector2>();
        // _move.Normalize();
    }

    public void OnSwap(InputValue value)
    {
    }

    void SwapInput(string _input) ///////////////////////////////////////////////////////
    {
        if (_input == currentControl) { return;  }
        currentControl = _input;
        if (_input == "KBM")
        {
            InputDevice[] devices = new InputDevice[2];
            devices[0] = Mouse.current;
            devices[1] = Keyboard.current;
        }
        else
        {
            InputDevice[] devices = new InputDevice[1];
            devices[0] = Gamepad.current;
        }
        playerInput.user.ActivateControlScheme(currentControl);
        playerInput.SwitchCurrentControlScheme(currentControl);
        GameManager.instance.UpdateUIByInputDevice(currentControl);
    }

    public void OnLook(InputValue value)
    {
        _look = value.Get<Vector2>();
    }

    public void OnToggleRun(InputValue value)
    {
        isRunning = !isRunning;
        currentControl = playerInput.currentControlScheme;
    }

    public void OnInteract(InputValue value)
    {
        Debug.Log("Interacting....");
        if (nearbyObject != null)
        {
            //animator.Play(""); -- aqui vai ser tipo: animator.Play(nearbyObject.anim) ou
            //eu faço uma funçao pra transformar o .anim em uma string, se n der certo
            //play ou settrigger
            // Debug.Log("Interacting with " + nearbyObject.name + " - playing animation: " + nearbyObject.anim);
            itemAnim = nearbyObject.anim;
            StartCoroutine("ItemCutscene");

            // GameManager.instance.CheckItems();
        }

        if (nearbyAnimal != null)
        {
            nearbyAnimal.DropItem();
            animator.SetTrigger("Bark");
        }

        if (isNextToWitch)
        {
            GameManager.instance.CheckItems();
            //witch.StartIdle();
        }
        currentControl = playerInput.currentControlScheme;
    }

    public void OnInventoryKey(InputValue value)
    {
        Inventory.instance.ToggleInventory();
        currentControl = playerInput.currentControlScheme;
    }

    public void OnCameraSpeed(InputValue value)
    {
        float _cameraSpeed = value.Get<float>();
        rotationPower += _cameraSpeed * 10;
    }

    void CallAnimation()
    {

    }

    public void ChangeParticles(ParticleSystem newPS)
    {
        foreach (ParticleSystem ps in allPS)
        {
            ps.Stop();
        }
        newPS.Play();
        currentPS = newPS;
    }

    IEnumerator ItemCutscene()
    {
        playerInput.DeactivateInput();
        GameManager.instance.inCutscene = true;
        playerCamera.SetActive(false);
        CinemachineFreeLook playerCM = playerCamera.GetComponent<CinemachineFreeLook>();

        //if (playerCM.m_XAxis.Value > -120f && playerCM.m_XAxis.Value < 60f)
        //{
        cutsceneCamera.SetActive(true);

        //} else { 
        //    cutsceneCamera2.SetActive(true);
        //}
        yield return new WaitForSeconds(2f);

        switch (itemAnim)
        {
            case Animacoes.Barking:
                animator.SetTrigger("Bark");
                break;
            case Animacoes.Digging:
                animator.SetTrigger("Dig");
                break;
        }
        nearbyObject.Disappear();
        yield return new WaitForSeconds(1f);
        Inventory.instance.AddToInventory(nearbyObject.item);
        // Destroy(nearbyObject.gameObject);
        nearbyObject = null;
        yield return new WaitForSeconds(1f);
        playerCamera.SetActive(true);
        cutsceneCamera.SetActive(false);
        cutsceneCamera2.SetActive(false);
        GameManager.instance.inCutscene = false;
        playerInput.ActivateInput();


    }


}
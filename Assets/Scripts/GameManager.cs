using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public Potion[] allPotions;
    public Potion currentPot;
    public List<ItemSlot> slots;
    public List<Image> slotsGFX;
    public int currentLevel = 1;
    public CinemachineFreeLook houseCam;
    public Transform witchPos;
    public GameObject explosionFx;
    public Transform caldeiraoPos;
    public Transform playerPos;
    public bool inCutscene = false;
    public CanvasGroup inventoryCG;
    public float timeSinceLevelStart = 0f;
    public float hurryUpThreshold = 15f;
    public Text witchText;
    public Image witchImage;
    public Sprite witchMad, witchCrazy, witchHappy;
    public GameObject dialogueBox;
    bool hurryUpAppeared = false;
    public GameObject[] blockers;
    // public List<

    [Header("UI")]
    public Text inventoryInputText;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        currentPot = allPotions[0];
        UpdateSlots();
        UpdateSilhouettes();
    }

    private void Update()
    {
        timeSinceLevelStart += Time.deltaTime;
        if (timeSinceLevelStart >= hurryUpThreshold && !hurryUpAppeared)
        {
            WitchPopUp("HURRY UP!", witchMad);
            hurryUpAppeared = true;
        }
    }

    public void CheckItems()
    {
        int itemCount = 0;
        switch (currentLevel)
        {
            case 1:
                foreach (InventoryItem i in allPotions[0].ingredients)
                {
                    if (Inventory.instance.inventory.Contains(i))
                    {
                        itemCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (itemCount == allPotions[0].ingredients.Length)
                {
                    ChangeLevel();
                }
                break;
        }
    }

    public void ChangeLevel()
    {
        Debug.Log("Passou de nivel!");
        currentLevel++;
        UpdateBlockers(currentLevel);
        currentPot = allPotions[currentLevel - 1];
        StartCoroutine("ChangingLevel");

    }

    public void UpdateSlots()
    {
        for (int i = 0; i < slots.Count; i++)
        {

            if (i == currentPot.ingredients.Length) { break; }
            slots[i].gameObject.SetActive(true);
            slots[i].item = currentPot.ingredients[i];
        }
        for (int i = currentPot.ingredients.Length; i < slots.Count; i++)
        {
            slots[i].gameObject.SetActive(false);
        }
    }

    public void UpdateSilhouettes()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if (i == currentPot.ingredients.Length) { break; }
            slotsGFX[i].gameObject.SetActive(true);
            slotsGFX[i].sprite = currentPot.ingredients[i].silhueta;
        }
        for (int i = currentPot.ingredients.Length; i < slots.Count; i++)
        {
            slotsGFX[i].gameObject.SetActive(false);
        }
    }

    public void UpdateUI()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            for (int j = 0; j < Inventory.instance.inventory.Count; j++)
            {
                if (slots[i].item.item == Inventory.instance.inventory[j].item)
                {
                    slotsGFX[i].sprite = Inventory.instance.inventory[j].img;
                }
            }
        }
    }

    IEnumerator ChangingLevel()
    {
        UpdateBlockers(currentLevel);
        inCutscene = true;
        yield return null;
        yield return new WaitForSeconds(1f);
        WitchPopUp("GOOD BOY!", witchHappy);
        houseCam.m_LookAt = witchPos;
        yield return new WaitForSeconds(2.5f);
        Instantiate(explosionFx, caldeiraoPos);
        WitchPopUp("MUAHAHAHAH!", witchCrazy);
        yield return new WaitForSeconds(5f);
        UpdateSilhouettes();
        houseCam.m_LookAt = playerPos;
        inCutscene = false;
        dialogueBox.SetActive(false);
    }

    public void ShowInventory()
    {
        Inventory.instance.ShowInventory();
    }

    public void UpdateBlockers(int level){
        blockers[level-2].SetActive(false);
    }

    public void WitchPopUp(string text, Sprite face)
    {
        dialogueBox.SetActive(true);
        witchText.text = text;
        witchImage.sprite = face;
    }

    IEnumerator InventoryFade()
    {
        float a = 0f;
        while (a <= 1f)
        {
            a += Time.deltaTime;
            inventoryCG.alpha = a;
            yield return null;
        }
        yield return new WaitForSeconds(3f);
        while (a >= 0f)
        {
            a -= Time.deltaTime;
            inventoryCG.alpha = a;
            yield return null;
        }
        yield return null;
    }

    IEnumerator InventoryAppear()
    {
        RectTransform rt = inventoryCG.GetComponent<RectTransform>();
        rt.pivot = new Vector2(1, 0);
        rt.anchoredPosition = Vector2.zero;
        rt.pivot = new Vector2(1, 1);
        Debug.Log(rt.anchoredPosition);
        while (rt.anchoredPosition.y > 0)
        {
            Debug.Log("Aaaaaaaaaaaaaa");
            rt.anchoredPosition = Vector2.Lerp(rt.anchoredPosition, Vector2.zero, Time.deltaTime);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        rt.anchoredPosition = Vector2.zero;
        rt.pivot = new Vector2(1, 0);
        while (rt.anchoredPosition.y < 0)
        {
            rt.anchoredPosition = Vector2.Lerp(rt.anchoredPosition, Vector2.zero, Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    public void UpdateUIByInputDevice(string currentDevice)
    {
        if (currentDevice == "KBM")
        {
            inventoryInputText.text = "I";
        }
        else
        {
            inventoryInputText.text = "Y";
        }
    }
}

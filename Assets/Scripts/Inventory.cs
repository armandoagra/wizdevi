using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory instance;
    public List<InventoryItem> inventory;
    public Animator animator;
    bool isInventoryOpen = false;

    private void Awake() {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearInventory(){
        inventory.Clear();
    }

    public void AddToInventory(InventoryItem item){
        inventory.Add(item);
        GameManager.instance.UpdateUI();
        Inventory.instance.CycleInventory();
    }

    public void ToggleInventory(){
        if (isInventoryOpen) { HideInventory(); }
        else { ShowInventory(); }
    }

    public void ShowInventory(){
        animator.Play("Show", 0);
        isInventoryOpen = true;
    }

    public void HideInventory(){
        isInventoryOpen = false;
        animator.Play("Hide", 0);
    }

    public void CycleInventory(){
        ShowInventory();
        Invoke("HideInventory", 3.5f);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
public class Sky : MonoBehaviour
{
    [Tooltip("Em horas (jogo) por segundo (real)")]
    public float velocidadeTempo;
    public float velocidadeRotacao;
    public Color manha, tarde, noite; //FFFFFF, FFD9A5, 575757
    ColorParameter manhaCP, tardeCP, noiteCP, startCP, currentCP, targetCP;

    public Texture manhaTx, tardeTx, noiteTx;
    public float currentTime = 6f;
    public Momento momento;
    public float blend;
    public Color startColor;
    public Color current;
    public Color targetColor;
    public Volume volume;
    public ColorAdjustments ca;
    Color currentColor;
    public ParticleSystem[] vagalumes;
    bool vagalumesAreActive;
    // ColorParameter cp;


    public enum Momento
    {
        Manha,
        Tarde,
        Noite
    }

    void Start()
    {
        momento = Momento.Manha;
        RenderSettings.skybox.SetTexture("_Tex1", manhaTx);
        RenderSettings.skybox.SetTexture("_Tex2", tardeTx);
        RenderSettings.skybox.SetFloat("_Blend", 0f);
        manhaCP = new ColorParameter(manha, true);
        tardeCP = new ColorParameter(tarde, true);
        noiteCP = new ColorParameter(noite, true);
        currentCP = new ColorParameter(manha, true);
        blend = 0f;
        targetColor = tarde;
        startColor = manha;
        // RenderSettings.ambientLight = manha;
        VolumeProfile profile = volume.sharedProfile;
        ColorAdjustments tmp;
        if (profile.TryGet<ColorAdjustments>(out tmp))
        {
            ca = tmp;
        }
        // ca.colorFilter = new ColorParameter(startColor, false);
        // ca.colorFilter.overrideState = false;//

    }


    // if (!profile.TryGet<ColorAdjustments>(out var ca)){

    // }


    // Update is called once per frame
    void Update()
    {
        blend += Time.deltaTime * velocidadeTempo / 8f;
        currentTime += Time.deltaTime * velocidadeTempo;
        // Debug.Log("Hora atual: " + (int)currentTime + ":00");
        if (currentTime > 24f)
        {
            currentTime = 0f;
        }
        ChangeTimeOfDay();
        RenderSettings.skybox.SetFloat("_Blend", blend);
        currentColor = Color.Lerp(startColor, targetColor, blend);
        ca.colorFilter.value = currentColor;

    }

    public void ChangeTimeOfDay()
    {
        if (currentTime > 6f && currentTime < 14f)
        {
            if (momento != Momento.Manha)
            {
                momento = Momento.Manha;
                RenderSettings.skybox.SetTexture("_Tex1", manhaTx);
                RenderSettings.skybox.SetTexture("_Tex2", tardeTx);
                RenderSettings.skybox.SetFloat("_Blend", 0f);
                blend = 0f;
                startColor = manha;
                targetColor = tarde;
                currentCP = new ColorParameter(manha, true);
                vagalumesAreActive = false;
                ToggleVagalumes(vagalumesAreActive);
            }
        }
        else if (currentTime > 14f && currentTime < 22f)
        {
            if (momento != Momento.Tarde)
            {
                momento = Momento.Tarde;
                RenderSettings.skybox.SetTexture("_Tex1", tardeTx);
                RenderSettings.skybox.SetTexture("_Tex2", noiteTx);
                RenderSettings.skybox.SetFloat("_Blend", 0f);
                blend = 0f;
                startColor = tarde;
                targetColor = noite;
                currentCP = new ColorParameter(tarde, true);
            }
        }
        else if (currentTime > 22f || currentTime < 6f)
        {
            if (momento != Momento.Noite)
            {
                momento = Momento.Noite;
                RenderSettings.skybox.SetTexture("_Tex1", noiteTx);
                RenderSettings.skybox.SetTexture("_Tex2", manhaTx);
                RenderSettings.skybox.SetFloat("_Blend", 0f);
                blend = 0f;
                startColor = noite;
                targetColor = manha;
                currentCP = new ColorParameter(noite, true);
            }
        }
        if (currentTime > 18f && !vagalumesAreActive)
        {
            vagalumesAreActive = true;
            ToggleVagalumes(vagalumesAreActive);
        }
        

    }

    void ToggleVagalumes(bool active)
    {
        Debug.Log("Vagalumes are now... " + active);
        foreach (ParticleSystem ps in vagalumes)
        {
            ParticleSystem.EmissionModule em = ps.emission;
            if (active)
            {
                em.enabled = true;
            }
            else
            {
                em.enabled = false;
            }
        }
    }
}





using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem.Controls;
using System.Linq;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    public int selectedButton;
    public Animator[] buttonAnims;
    PlayerInput input;
    public int qtdeBotoes;
    public GameObject creditsPage, exitButton;
    public AudioSource hoverSound;
    public AudioSource confirmSound;
    public AudioSource playSound;
    AsyncOperation asyncLoad;
    public int clickedButton;
    public bool cursorIsActive;
    InputControl[] blockedButtons;
    bool creditsIsActive;
    bool started;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<PlayerInput>();
        ToggleAnimators();
        StartCoroutine(LoadAsyncScene());
        if (Gamepad.current != null)
        {
            blockedButtons = new InputControl[8] {
            Gamepad.current.leftStick.left,  Gamepad.current.leftStick.right,
            Gamepad.current.leftStick.up, Gamepad.current.leftStick.down,
            Gamepad.current.rightStick.left, Gamepad.current.rightStick.right,
            Gamepad.current.rightStick.up, Gamepad.current.rightStick.down
        };
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Keyboard.current.anyKey.wasPressedThisFrame)
        {
            Cursor.visible = true;
        }
        if (Mouse.current.leftButton.wasPressedThisFrame || Mouse.current.rightButton.wasPressedThisFrame)
        {
            Cursor.visible = true;
        }
        if (Gamepad.current != null)
        {
            Debug.Log(Gamepad.current.allControls.Count);
            for (int i = 0; i < Gamepad.current.allControls.Count; i++)
            {
                InputControl c = Gamepad.current.allControls[i];
                if (c is ButtonControl)
                {
                    if (((ButtonControl)c).wasPressedThisFrame)
                    {
                        Cursor.visible = false;
                    }
                }
            }
        }
    }

    public void OnChangeSelection(InputValue value)
    {
        float _direction = value.Get<float>();
        int newSelection = (int)_direction;
        newSelection = newSelection + selectedButton;
        if (newSelection < 0)
        {
            newSelection = qtdeBotoes - 1;
        }
        else if (newSelection >= qtdeBotoes)
        {
            newSelection = 0;
        }
        UpdateSelected(newSelection);
    }

    public void OnButtonClick(InputValue value)
    {
        float _buttonPressed = value.Get<float>();
        Debug.Log("Clicou no botao " + selectedButton);
        HandleButtonAction(selectedButton);
    }

    void ToggleAnimators()
    {
        for (int i = 0; i < buttonAnims.Length; i++)
        {
            buttonAnims[i].SetBool("Active", false);
            buttonAnims[i].GetComponent<MeshRenderer>().enabled = false;
        }
        buttonAnims[selectedButton].SetBool("Active", true);
        buttonAnims[selectedButton].GetComponent<MeshRenderer>().enabled = true;
    }

    public void UpdateSelected(int _selected)
    {
        if (creditsIsActive || started) { return;  }
        hoverSound.Play();
        selectedButton = _selected;
        ToggleAnimators();
    }

    public void HandleButtonAction(int _pressed)
    {

        switch (_pressed)
        {
            case 0:
                playSound.Play();
                started = true;
                StartCoroutine(PlayDelay());
                break;
            case 1:
                creditsPage.SetActive(true);
                exitButton.SetActive(false);
                UpdateSelected(3);
                creditsIsActive = true;
                confirmSound.Play();
                break;
            case 2:
                confirmSound.Play();
                StartCoroutine(QuitDelay());
                break;
            case 3:
                confirmSound.Play();
                exitButton.SetActive(true);
                creditsIsActive = false;
                creditsPage.SetActive(false);
                UpdateSelected(1);
                break;
        }
    }

    IEnumerator PlayDelay()
    {
        yield return new WaitForSeconds(1f);
        asyncLoad.allowSceneActivation = true;
    }

    IEnumerator QuitDelay()
    {
        yield return new WaitForSeconds(.5f);
        Application.Quit(0);
    }

    IEnumerator LoadAsyncScene()
    {
        asyncLoad = SceneManager.LoadSceneAsync(1);
        asyncLoad.allowSceneActivation = false;
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void ButtonClick(int _btnID)
    {
        clickedButton = _btnID;
    }
}

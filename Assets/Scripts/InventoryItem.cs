using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Item", fileName="Item")]
public class InventoryItem : ScriptableObject
{

    public string nome;
    public Sprite silhueta;
    public Sprite img;
    public Items.items item;

}

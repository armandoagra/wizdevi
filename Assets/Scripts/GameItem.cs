using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItem : MonoBehaviour
{

    public PlayerMovement.Animacoes anim;
    public InventoryItem item;
    public MeshRenderer meshRenderer;
    Material mat;
    // Start is called before the first frame update
    void Start()
    {
        // meshRenderer = GetComponentInChildren<MeshRenderer>()
        Debug.Log(meshRenderer);
        Debug.Log(meshRenderer.name);
        mat = meshRenderer.material;
        mat.SetFloat("_DitherSize", 0f);
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerMovement>().nearbyObject = this;
        }
    }

    public void Disappear()
    {
        StartCoroutine("Disappearing");
    }

    IEnumerator Disappearing()
    {
        float opac = mat.GetFloat("_Opacity");
        mat.SetFloat("_DitherSize", 1f);
        while (opac > 0f)
        {
            transform.Translate(Vector3.up * Time.deltaTime * 0.8f);
            opac -= Time.deltaTime / 2f;
            mat.SetFloat("_Opacity", opac);
            yield return null;
        }
        Destroy(gameObject);
    }
}

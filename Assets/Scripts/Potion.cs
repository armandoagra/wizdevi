using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Potion", fileName="Potion")]
public class Potion : ScriptableObject
{
    public int level;
    public InventoryItem[] ingredients;
}

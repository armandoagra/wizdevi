using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHover : MonoBehaviour
{
    public int buttonID;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter() {
        if (Cursor.visible)
        {
            MenuManager.instance.UpdateSelected(buttonID);
        }
    }

    private void OnMouseDown()
    {
        if (Cursor.visible)
        {
            MenuManager.instance.ButtonClick(buttonID);
        }
    }

    private void OnMouseUp()
    {
        if (MenuManager.instance.clickedButton == buttonID && Cursor.visible)
        {
            MenuManager.instance.HandleButtonAction(buttonID);
        }
    }


}

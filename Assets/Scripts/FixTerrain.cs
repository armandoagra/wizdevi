using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTerrain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TerrainCollider tc = GetComponent<TerrainCollider>();
        tc.enabled = false;
        tc.enabled = true;
    }

}

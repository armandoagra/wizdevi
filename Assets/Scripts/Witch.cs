using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Witch : MonoBehaviour
{
    public Animator anim;
    public Witch witch;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartIdle(){
        anim.SetTrigger("Idle");
    }

    public void StartMixing(){
        anim.SetTrigger("Mixing");
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().isNextToWitch = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().isNextToWitch = false;
            // witch.StartMixing();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{

    public Image image;
    public InventoryItem item;

    private void Start() {
        image = GetComponent<Image>();
    }

}

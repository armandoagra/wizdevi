using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseCheck : MonoBehaviour
{
    public Witch witch;
    public GameObject camExterna;
    public GameObject camInterna;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().insideHouse = true;
            camInterna.SetActive(true);
            camExterna.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other) {
         if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().insideHouse = false;
            camInterna.SetActive(false);
            camExterna.SetActive(true);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chicken : MonoBehaviour
{
    public Transform[] waypoints;
    NavMeshAgent agent;
    bool isAfraid;
    public Animator animator;
    public bool droppedEgg;
    public float regularSpeed;
    public float scaredSpeed;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        InvokeRepeating("WalkAround", 5, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.velocity.magnitude < .1f)
        {
            animator.SetFloat("Movimento", 0);
        } else {
            if (!isAfraid){
                animator.SetFloat("Movimento", 0.5f);
            } else {
                animator.SetFloat("Movimento", 1f);
            }
        }
    }

    void WalkAround()
    {
        Transform nextPos = waypoints[Random.Range(0, waypoints.Length)];
        agent.SetDestination(nextPos.position);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CancelInvoke();
            isAfraid = true;
            agent.speed = scaredSpeed;
            other.GetComponent<PlayerMovement>().nearbyAnimal = GetComponent<Animal>();
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Player"))
        {
            ChooseNextPoint(other.transform.position);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            agent.speed = regularSpeed;
            isAfraid = false;
            InvokeRepeating("WalkAround", 5, 5f);
            other.GetComponent<PlayerMovement>().nearbyAnimal = null;
        }
    }

    void ChooseNextPoint(Vector3 playerPos){
        float dist = 0;
        Vector3 nextPos = Vector3.zero;
        foreach (Transform t in waypoints){
            if (Vector3.Distance(t.position, playerPos) > dist){
                dist = Vector3.Distance(t.position, playerPos);
                nextPos = t.position;
            }
        }
        agent.SetDestination(nextPos);
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Items
{
    public enum items{
        BaldeAgua,
        Laranja,
        Flor1,
        Flor2,
        Flor3,
        Leite,
        Frutinha,
        Feno,
        Lama,
        AboboraPequena,
        Farinha,
        Ovo,
        Trevo,
        PeloSapo,
        RestoPocaoVelha,
        PlasmaFantasma,
        Osso, 
        RoupaEspantalho,
        TiticaMorcego,
        PlantaBrilhante,
        VitoriaRegia,
        BracoEspantalho,
        CeraVela,
        AsaMorcego,
        Galho,
        PenaGalinha,
        FrutoArvore,
        OleoTrator,
        ChaveFazenda,
        LagartaAbobora,
        Varinha,
        CranioCorvo,
        Tijolo,
        Trigo

    }
}

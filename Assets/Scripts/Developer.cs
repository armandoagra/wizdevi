using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Developer
{
    [MenuItem("Dev/Zone 1")]
    public static void StartZone1()
    {
        Debug.Log("Starting Zone 1...");
        Inventory.instance.ClearInventory();
        InventoryItem item = ScriptableObject.CreateInstance<InventoryItem>();
        Inventory.instance.AddToInventory(item);
    }
    
   }

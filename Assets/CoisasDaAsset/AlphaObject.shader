    Shader "Custom/AlphaObject"
    {
        Properties
        {
            _MainTex ("Texture", 2D) = "gray" {}
            _Ambience("Ambience", Float) = 1
        }
        SubShader
        {
            Tags{ "Queue" = "Transparent" "RenderType" = "TransparentCutout" }
            Blend SrcAlpha OneMinusSrcAlpha
            LOD 200
            ZWrite Off
     
            Pass
            {
                Cull Off
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
     
               #pragma target 5.0
                #include "UnityCG.cginc"
                #include "Lighting.cginc"
     
               // make fog work
               #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight multi_compile_fog
     
               // shadow helper functions and macros
               #include "AutoLight.cginc"
     
               struct VertexData {
                   float3 Vertex;
                   float2 UV;
                   float3 Normal;
               };
     
               uniform StructuredBuffer<VertexData> _Vertex;
     
               float _Ambience;
     
               struct v2f
               {
                   float2 uv : TEXCOORD0;
                   UNITY_FOG_COORDS(1)
                   SHADOW_COORDS(2) // put shadows data into TEXCOORD2
                   float4 pos : SV_POSITION;
                   fixed3 diff : COLOR0;
                   fixed3 ambient : COLOR1;
                   fixed3 diff_rev : COLOR2;
                   fixed3 ambient_rev : COLOR3;
                   float4 worldPos : TEXCOORD3;
     
                   half3 worldNormal : NORMAL;
                   half3 worldNormal_rev : NORMAL1;
               };
     
               sampler2D _MainTex;
               float4 _MainTex_ST;
     
                v2f vert(uint id : SV_VertexID)
                {
                   v2f o;
     
                   float4 in_v = float4(_Vertex[id].Vertex, 1);
     
                   o.pos = UnityObjectToClipPos(in_v);
                   o.uv = TRANSFORM_TEX(_Vertex[id].UV, _MainTex);
                   UNITY_TRANSFER_FOG(o, o.pos);
     
                   o.worldPos = mul(unity_ObjectToWorld, in_v);
     
                   half3 worldNorm = UnityObjectToWorldNormal(_Vertex[id].Normal);;
                 
     
                   // diffuse and ambient lighting.
                   half nl = max(0, dot(-worldNorm, _WorldSpaceLightPos0.xyz));
                   o.diff = nl * _LightColor0.rgb;
                   o.ambient = ShadeSH9(half4(-worldNorm, 1)) + _Ambience;
                   o.worldNormal = -worldNorm;
     
     
                   nl = max(0, dot(worldNorm, _WorldSpaceLightPos0.xyz));
                   o.diff_rev = nl * _LightColor0.rgb;
                   o.ambient_rev = ShadeSH9(half4(worldNorm, 1)) + _Ambience;
                   o.worldNormal_rev = worldNorm;
     
                   // compute shadows data
                   TRANSFER_SHADOW(o)
     
                   return o;
                }
     
                fixed4 frag(v2f i, half facing : VFACE) : SV_Target
                {
                   // sample the texture
                   //fixed4 col = fixed4(0.5, 0.5, 0.5, 0.5);
                   fixed4 col = tex2D(_MainTex, i.uv);
     
                   fixed3 diff = i.diff;
                   fixed3 ambient = i.ambient;
                   half3 normal = i.worldNormal;
     
                   if (facing < 0.5) {
                      normal = i.worldNormal_rev;
                      diff = i.diff_rev;
                      ambient = i.ambient_rev;
                   }
     
                   // compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
                   fixed shadow = SHADOW_ATTENUATION(i);
                   // darken light's illumination with shadow, keep ambient intact
                   fixed3 lighting = diff * shadow + ambient;
                   col.rgb *= lighting;
     
                   // apply fog
                   //UNITY_APPLY_FOG(i.fogCoord, col);
                   return col;
                }
                ENDCG
            }
        }
    }

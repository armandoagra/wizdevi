using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aveia : MonoBehaviour
{
    public ParticleSystem trigoPS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().inHighGrass = true;
            other.GetComponent<PlayerMovement>().ChangeParticles(trigoPS);
        }
    }

    // private void OnTriggerStay(Collider other) {
    //     if (other.CompareTag("Player")){
    //         other.GetComponent<PlayerMovement>().inHighGrass = true;
    //     }
    // }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            other.GetComponent<PlayerMovement>().inHighGrass = false;
            other.GetComponent<PlayerMovement>().ChangeParticles(other.GetComponent<PlayerMovement>().defaultPS);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDog : MonoBehaviour
{

    CharacterController cc;
    public Transform waypoint1, waypoint2;
    Transform waypointAtual;
    public float speed;
    int waypointAtualNum;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        waypointAtual = waypoint1;
        waypointAtualNum = 1;
        StartCoroutine(RunToSide());
        animator = GetComponentInChildren<Animator>();
        animator.SetFloat("Speed", 1f);
    }


    IEnumerator RunToSide()
    {
        
        transform.LookAt(waypointAtual);
        while (Vector3.Distance(transform.position, waypointAtual.position) > 3f)
        {
            cc.Move(transform.forward * speed * Time.deltaTime);
            yield return null;
        }
        if (waypointAtualNum == 1)
        {
            waypointAtual = waypoint2;
            waypointAtualNum = 2;
        } else
        {
            waypointAtual = waypoint1;
            waypointAtualNum = 1;
        }
        yield return new WaitForSeconds(2f);
        StartCoroutine(RunToSide());
    }
}
